# -*- coding: utf-8 -*-
"""
Created on Wed Jun 06 2018

@author: Ch.Kienholz
"""

import urllib2
import json
import pandas as pd
import datetime as dt
import scipy.optimize

#import seaborn as sb
import matplotlib 
import matplotlib.pyplot as plt

import numpy as np
import scipy
import matplotlib.dates as mdates

from bs4 import BeautifulSoup

import misc
import utilities as util

matplotlib.style.use('classic')

# set Arial as standard font
matplotlib.rc("font", **{"sans-serif": ["Arial"]}) # "size": fontsize}
    
#%% Classes to download/prepare water level and discharge data
       
class Suicide_Basin_Gauge(): 
    '''
    Class to download the Suicide Basin gauge data from the USGS denali 
    server and to store the data in a time series (local time, in m). 
    A correction is applied to arrive at gage elevations relative to the 
    ellipsoid. This function was used only during the 2018 season.
    '''

    def __init__(self):
        
        self.series_gage_height = None
        
    def download(self):
        
        # in m (= 28.48ft), to get water level relative to the ellipsoid, 
        # applies before 6/10/2018 
        correction1 = 8.68 
        
        # in m (= 1297.67ft), to get water level relative to the ellipsoid
        # applies after 6/10/2018
        correction2 = 395.53  
        
        time_zone = 'AKDT' # alaska daylight saving time (utc - 8 hours)
        
        # url of the servers
        url1 = ('http://denali.micro-specialties.com/cgi-bin/GlobalModemData.cgi?'
                'site=sn156&start=2018/05/20')
        url2 = ('http://denali.micro-specialties.com/cgi-bin/GlobalModemData.cgi?'
                'site=sn177&start=2018/05/26')
        
        timelist = []
        levellist = []
        
        for url in [url1, url2]:
        
            service = urllib2.urlopen(url) # open the URL
            data_to_parse = BeautifulSoup(service)
            
            for line in data_to_parse.get_text().split("\r\n"):
                
                line_elements = line.split(',')
                time_str = line_elements[0]
                
                try:
                    # create a time object
                    timeobj = dt.datetime.strptime(time_str, '%Y/%m/%d %H:%M')
                    
                    # decode the structure of the csv files
                    for position, mins in zip([1, 5, 9, 13], [0, 15, 30, 45]):
                        
                        level = float(line_elements[position]) * util.feet_to_m
                        
                        if level > 1: # logger has some zero values that need to be deleted
                            if time_zone == 'AKDT':
                                timelist.append(timeobj - dt.timedelta(minutes=mins) 
                                                - dt.timedelta(hours=8)) 
                            else: # UTC
                                timelist.append(timeobj - dt.timedelta(minutes=mins))
                                
                            levellist.append(level)
                        else:
                            pass
                except: # to catch headers, etc  
                    pass
            
        series_gage_height = pd.Series(levellist, index=timelist, name='gage_height')
        
        # split series into slices of good data and apply correction
        s1 = series_gage_height[:'06-05-2018 16:00'] + correction1 
        s2 = series_gage_height['06-10-2018 14:15':'06-10-2018 15:15'] + correction2
        s3 = series_gage_height['06-12-2018 11:15':'06-25-2018 13:00'] + correction2
        # recombine the slices into one  series        
        series_gage_height = pd.concat([s1, s2, s3])
           
        # create a complete time series
        # times without data will contain nans, which makes the plotting easier 
        mindate = min(series_gage_height.index)
        maxdate = max(series_gage_height.index)
        self.series_gage_height = series_gage_height.reindex(pd.date_range(mindate, 
                                                    maxdate, freq='15min'))
           
class Mendenhall_Lake_Gauge_Weather(object):
    '''
    Class to download Mendenhall runoff, gauge height, airtemp, and precip 
    from waterservices.usgs.gov
    '''    
    
    def __init__(self):
        
        self.series_runoff = None
        self.series_gage_height = None
        self.series_airtemp = None
        self.series_precip = None

    def json_dict_to_series(self, json_dict, name):    
        '''
        Method to convert json dictionary into a time series
        '''
        
        value = []
        datetime = []
        
        for item in json_dict:
            
            try:
                datetime.append(dt.datetime.strptime(item['dateTime'], 
                                                     '%Y-%m-%dT%H:%M:%S.000-08:00'))
            except:
                datetime.append(dt.datetime.strptime(item['dateTime'], 
                                                     '%Y-%m-%dT%H:%M:%S.000-09:00'))
            if name == 'gage_height':
                value.append(float(item['value']) * util.feet_to_m)
            elif name == 'runoff':
                value.append(float(item['value']) * util.feet3_to_m3)
            elif name == 'airtemp':
                value.append(float(item['value']))
            elif name == 'precip':
                value.append(float(item['value']) * util.inch_to_mm)
            else:
                raise ValueError('keyword {} not recoginized'.format(name))
        
        # create time series           
        series = pd.Series(value, index=datetime, name=name)
        
        return series    
    
    def download(self, days=30):

        # created using https://waterservices.usgs.gov/rest/DV-Test-Tool.html PT50H
        # downloads runoff, water level, air temp, and precip
        url = ('http://waterservices.usgs.gov/nwis/iv/?format=json&sites=15052500&period'
               '=P{}D&parameterCd=00020,00045,00060,00065&siteStatus=all').format(days) # ,00065
          
        service = urllib2.urlopen(url) # open the URL
        json_str = service.read() # read into json string
    
        # convert json string to dictionary
        # returns a list of dataframes with time and value (runoff or gage height)
        json_dict_runoff = json.loads(json_str)['value']['timeSeries'][2]['values'][0]['value']
        json_dict_wlevel = json.loads(json_str)['value']['timeSeries'][3]['values'][0]['value']
        json_dict_airtemp = json.loads(json_str)['value']['timeSeries'][0]['values'][0]['value']
        json_dict_precip = json.loads(json_str)['value']['timeSeries'][1]['values'][0]['value']
        
        self.series_runoff = self.json_dict_to_series(json_dict_runoff, 'runoff')
        self.series_gage_height = self.json_dict_to_series(json_dict_wlevel, 'gage_height')
        self.series_airtemp = self.json_dict_to_series(json_dict_airtemp, 'airtemp')
        self.series_precip = self.json_dict_to_series(json_dict_precip, 'precip')    

#%%

class Mendenhall_Lake_GLOF_Model_Parameters(object):
    '''
    Class that provides the input parameters for Mendenhall_Lake_GLOF_Model.
    Adapt parameters as needed.
    testmode = 1 allows for script testing, using 2019 data as input.
    '''
    
    def __init__(self):
        
        testmode = 1 # (1) load provided 2019 data to test the script, (0) download current data from usgs server
        
        self.glof_start = pd.to_datetime('2020-07-12 23:30') # time when the inflow curve starts rising 
        self.current_time = pd.datetime.now() 
        
        self.baseflow_text = 1 # (1) load baseflow data from textfile, (0) automatically create linear baseflow curve  
        self.baseflow_text_path = './data/your_baseflow.csv' # only needed in case baseflow_text == 1
        
        # create instance to download gauge and weather data from the usgs server
        Mendenhall_series = Mendenhall_Lake_Gauge_Weather()
        
        if testmode == 0:
            Mendenhall_series.download(days=10) # download data from the last 10 days        
        
        else: # load pre-downloaded series for script testing 
            Mendenhall_series.series_runoff = pd.read_pickle('./data/test_2019/series_runoff_menden_2019.pickle')
            Mendenhall_series.series_gage_height = pd.read_pickle('./data/test_2019/series_gage_height_menden_2019.pickle')
            Mendenhall_series.series_airtemp = pd.read_pickle('./data/test_2019/series_airtemp_menden_2019.pickle')
            Mendenhall_series.series_precip = pd.read_pickle('./data/test_2019/series_precip_menden_2019.pickle') 

            # overwrite some of the parameters
            self.glof_start = pd.to_datetime('2019-07-12 23:30') 
            self.current_time = pd.to_datetime('2019-07-14 10:00')
  
            self.baseflow_text = 1
            self.baseflow_text_path = './data/test_2019/baseflow_2019.csv'
                  
        self.series_runoff_menden = Mendenhall_series.series_runoff  
        self.series_gage_height_menden = Mendenhall_series.series_gage_height 
        self.series_airtemp_menden = Mendenhall_series.series_airtemp
        self.series_precip_menden = Mendenhall_series.series_precip
        
        #---------------------
        
        self.mc = 1 # switch for monte carlo simulations yes (1) or no (0)
        
        if self.mc == 1: 
            # how many monte carlo simulations to run
            self.mc_nr = 1000
            # how many hours of the inflow time series to use to fit the exponential
            self.hours_overlap_range = [5, 10]
            # how much of the water drains during the rising limb (0.74 is 74%)
            self.percentage_rising = [0.74, 0.96] 
            # vary the derived slope of the inflow curve within this value, 0.2 = 20%
            self.slope_change = 0.2 
            # apply maximum slope change at this time (hours)
            self.slope_change_time = [48, 72]
            # by how much to vary the glof volume (0.1 is +-10%)
            self.glof_volume_var = 0.1 
            # by how much to vary the baseflow (0.3 is +-30%) 
            self.baseflow_var = 0.3            
    
        else:
            self.mc_nr = 1
            self.hours_overlap = 10
            self.percentage_rising = 0.78 
        
        # for how many days to run the model
        self.model_end = self.glof_start + dt.timedelta(days=6)
        
        # load lookup tables to convert between discharge and Mendenhall 
        # Lake elevation and vice versa
        self.runoff_vs_elev = pd.read_pickle('./data/runoff_vs_elev.pickle')
        self.elev_vs_runoff = pd.read_pickle('./data/elev_vs_runoff.pickle')
    
        # load lookup table that contains the Mendenhall Lake areas as a function of the water level     
        self.elev_vs_area_mendenhall = pd.read_pickle('./data/mendenhall_area_lookup.pickle')

        # switch for log y axis (1) or not (0) on final plots
        self.log = 0         
        
        # x-extent for final plots                                     
        self.fig_mindate = self.current_time.date() - dt.timedelta(days=5)
        self.fig_maxdate = self.current_time.date() + dt.timedelta(days=5)


class Mendenhall_Lake_GLOF_Model(Mendenhall_Lake_GLOF_Model_Parameters):                    
    '''
    Class predicting the water inflow into Mendenhall Lake for a given GLOF volume
    and baseflow. The predicted inflow curve portion is derived from the most 
    recent observed slope of the inflow curve, by fitting an exponential.
    Several parameters are varied as part of the MC calculations.
    The outflow from Mendenhall Lake is calculated via storage function.
    '''

    def __init__(self):
        
        # inherit the model parameters from the parameter class
        Mendenhall_Lake_GLOF_Model_Parameters.__init__(self)
               
        #------------------
        # rolling mean, resample to minutes, and linear interpolation for nans
        # discharge
        self.series_runoff_rm = misc.rolling_mean_df(self.series_runoff_menden, elements=15)
        self.series_runoff_rm = misc.timeseries_to_minutes(self.series_runoff_rm, method='linear')
        # gauge height
        series_gage_height_rm = misc.rolling_mean_df(self.series_gage_height_menden, elements=15)
        series_gage_height_rm = misc.timeseries_to_minutes(series_gage_height_rm, method='linear')
        
        # call the calc_inflow function using the smoothed discharge and gauge height series
        inflow_series = util.calc_inflow(self.series_runoff_rm, series_gage_height_rm, 
                                         lake_area=self.elev_vs_area_mendenhall, smoothing=1, 
                                         smoothing_len=120)
        
        # cut the last two hours (smoothing doesn't work well here)
        cutoff_time = self.current_time - dt.timedelta(minutes=239) # inflow_series.index[-1] - dt.timedelta(minutes=239)
        self.inflow_series = inflow_series[:cutoff_time]
                
        self.fit_end_time = cutoff_time
        
        # cut glof inflow series at the glof start time
        self.inflow_series_glof = self.inflow_series[self.glof_start:]     
        
        #----------------------
        # code to create a baseflow series        
        # set first baseflow value to the value of the inflow_series at the glof_start time
        self.base_inflow_val = self.inflow_series[self.glof_start]
    
        if self.baseflow_text == 1:
            # load textfile with predicted baseflow (e.g. passed from NOAA model)
            bsf = pd.read_csv(self.baseflow_text_path)
            # set the baseflow value at glof start to the value of the inflow
            date_list = [self.glof_start] + list(bsf['date']) 
            self.flow_list = [self.base_inflow_val] + list(bsf['flow'])         
        
        else:
            # create a constant baseflow curve from scratch
            # additional values can be added between first and last value if needed
            # working with an actual text file is recommended for this task
            date_list = [self.glof_start, self.model_end] # '2019-07-16 00:00', '2019-07-18 12:00', self.model_end  
            self.flow_list = [self.base_inflow_val, self.base_inflow_val] # 70, 85, self.base_inflow_val
        
        # convert date strings to datetime
        self.date_list = [pd.to_datetime(date) for date in date_list]
        
        # create a copy of the original baseflow list (to be used in the MC simulations)
        self.flow_list_orig = np.array(self.flow_list).copy()
        
    def runoff_fit_fun_mc(self, t, a, b):
        '''
        Function contains exponential to be fitted to data. 
        Function also varies the slope of the curve.
        Adaptation of the factor is implemented as linear function, 
        with no change at t = 0 and maximum change between 48 and 72 hours.     
        '''
        
        # random sampling for slope increase 1 + x is multiplied with actual slope 
        pool_1 = np.arange(-self.slope_change, self.slope_change, 0.01)
        choice_1 = np.random.choice(pool_1)
        
        # random sampling for time
        pool_2 = np.arange(self.slope_change_time[0], self.slope_change_time[1])
        choice_2 = np.random.choice(pool_2)
        
        # factor implemented as a linear function
        factor = 1 + (t - self.hours_overlap) * (choice_1 / choice_2)
        adapted =  factor * a * np.exp(b * t)
        
        # make sure curve is continuous
        # subtract value at transition (i.e, at time == overlap_hours)
        diff = factor * a * np.exp(b * self.hours_overlap) - a * np.exp(b * self.hours_overlap)
    
        return adapted - diff

    def extend_runoff_series(self):    
        '''
        Function to extend the inflow series into the future, using an 
        exponential fit to the data
        '''
        
        start_time = self.fit_end_time - dt.timedelta(hours=self.hours_overlap)
        
        # isolate the inflow series portion on which to conduct the curve fitting
        series_sel = self.inflow_series_glof[start_time:self.fit_end_time]
        
        # convert to delta hours (scipy.optimize.curve_fit does not work with 
        # datetimes)
        delta_hours = misc.time_to_delta_hours(series_sel) 
        
        # exponential to be fitted to data              
        runoff_fit_fun = lambda t, a, b: a * np.exp(b * t)
        
        # calculate the curve_fit
        popt, pcov = scipy.optimize.curve_fit(runoff_fit_fun, delta_hours, series_sel)
        
        # extend delta hours into the future  
        extension_time = 5 * 24
        delta_hours_ext = np.arange(delta_hours.max(), delta_hours.max() + extension_time, 
                                    delta_hours[1] - delta_hours[0])
        
        # convert delta hours back to datetimes
        time_index = misc.delta_hours_to_time(delta_hours_ext, start_time=series_sel.index[0])
        
        if self.mc == 1:
            discharge = self.runoff_fit_fun_mc(delta_hours_ext, popt[0], popt[1])
        else:
            discharge = runoff_fit_fun(delta_hours_ext, popt[0], popt[1])
            
        series_extended = pd.Series(discharge, index=time_index) 
        
        return series_extended        
           
    def forecast_inflow(self):
                
        series_extended = self.extend_runoff_series() 
        
        # append the predicted inflow series to the measured series
        series_extended_concat = pd.concat([self.inflow_series_glof[:self.fit_end_time], 
                                            series_extended.iloc[1:]])
        series_extended_concat.name = 'discharge'
        
        # check if the extended inflow series or the baseflow series is longer
        # if baseflow series is longer, cut it to the exent of the extended inflow series 
        if series_extended.index[-1] < self.baseflow_series.index[-1]:
            
            baseflow_series = self.baseflow_series[:series_extended.index[-1]] 
        
        # if the extended inflow series is longer, extend baseflow series by adding the baseflow end val     
        else:
            
            baseflow_series_extension = series_extended_concat[self.baseflow_series.index[-1]:][1:]
            baseflow_series_extension[:] = self.baseflow_series[-1] # allocate baseflow_end_val 
            baseflow_series = pd.concat([self.baseflow_series, baseflow_series_extension], axis=0)
            
        # create runoff cumsum and subtract cumsum of base runoff    
        series_extended_concat_cumsum = series_extended_concat.cumsum() - baseflow_series.cumsum()
        series_extended_concat_cumsum.name = 'discharge_cumsum'
                 
        # create dataframe with discharge and cumsum discharge
        series_concat = pd.concat([series_extended_concat, series_extended_concat_cumsum], axis=1)
        
        # allocate a certain percentage of the GLOF volume to the rising limb 
        if self.mc == 1:
            percentage = misc.sample_uniform(self.percentage_rising)   
        else:
            percentage = self.percentage_rising 
        
        glof_volume_up = percentage * self.glof_volume
    
        # identify peak discharge and time of peak discharge
        # also multiply with 60 to account for the fact that the series contain measurements 
        # every minute rather than not every second
        peak_discharge = series_concat.loc[((series_concat['discharge_cumsum']) 
                                          * 60 <= glof_volume_up), 'discharge'][-1]
        glof_volume_up_rl = series_concat.loc[((series_concat['discharge_cumsum']) 
                                             * 60 <= glof_volume_up), 'discharge_cumsum'][-1] * 60
        peak_time = series_concat.loc[((series_concat['discharge_cumsum']) 
                                     * 60 <= glof_volume_up)].index[-1]
        
        glof_volume_down = self.glof_volume - glof_volume_up_rl 
                
        if peak_time < self.fit_end_time:
            return [None, 0]
            
        else:
            #------------------------------------------------------------------------------------------------
            # the following calculations assume a linear decrease of flow between the peak flow and base flow
            # in order to conserve water volume, the slope of this curve needs to be determined,
            # using trigonometry (the area of the triangle must match the glof volume)
            # if the baseflow is constant, no iterative approach is required
            # if the baseflow is not constant, an iterative approach is required because
            # there are too many unknowns (slope of falling limb affects the amount of baseflow)
            
            # difference between peak flow and base flow
            # for first iteration, assume the baseflow is constant at level of peak time
            baseflow_mean_prior = baseflow_series[peak_time]
            flow_diff = peak_discharge - baseflow_mean_prior
    
            # assume linear decrease of flow with a 90 degree angle
            minutes_remaining = round((glof_volume_down * 2.0) / (flow_diff * 60), 0)
            
            # random large volume difference
            volume_difference = 10 ** 10
            volume_diff_prev = volume_difference + 10
            n = 0         
            
            while n < 100: # allow up to 100 iterations
                       
                # determine intersection time
                time_intersect = peak_time + dt.timedelta(minutes=minutes_remaining)
                
                series_concat_sel = series_concat.loc[(series_concat.index <= peak_time) 
                                                     | (series_concat.index >= time_intersect)]['discharge'] 
                
                # set series after the intersection time to the base flow values
                series_concat_sel[time_intersect:] = baseflow_series[time_intersect:]
                
                # interpolate to minutes, which introduces a linear drop between peak and intersection time
                series_concat_sel = misc.timeseries_to_minutes(series_concat_sel, method='linear')


                # calculate the volume difference between the target glof_volume_down
                # and the actual glof_volume_down
                # multiply by 60 to account for the fact that we work on a time series
                # at minute spatial resolution
                volume_difference = glof_volume_down - 60 *(series_concat_sel[peak_time:time_intersect].cumsum()[-1] 
                                                            - baseflow_series[peak_time:time_intersect].cumsum()[-1])
                
                # print volume_difference
                # print time_intersect
                
                # adapt in one minute steps                
                if volume_difference > 0:
                    minutes_remaining = minutes_remaining + 1
                    
                else:
                    minutes_remaining = minutes_remaining - 1
                
                # check whether the volume difference improves
                if abs(volume_difference) - volume_diff_prev >= 0:
                    # print 'no improvement... break'
                    time_intersect = time_intersect_prev
                    break
                    
                volume_diff_prev = abs(volume_difference)
                time_intersect_prev = time_intersect
                    
                n += 1
            
            time_intersect = peak_time + dt.timedelta(minutes=minutes_remaining)
            
            # retain only the series portions before peak time and after intersection time
            series_concat = series_concat.loc[(series_concat.index <= peak_time) | 
                                              (series_concat.index >= time_intersect)]['discharge'] 
                   
            # set series after the intersection time to the baseflow values
            series_concat[time_intersect:] = baseflow_series[time_intersect:]
             
            # cut series to predefined length (5 days after intersection time)
            time_limit = time_intersect + dt.timedelta(days=5)
            series_concat_reduced = series_concat[:time_limit]
            
            # resample to minutes (the linear falling limb does not yet have minute intervals)
            series_concat_reduced = misc.timeseries_to_minutes(series_concat_reduced, method='linear')
            
            # rolling mean over 2 hours
            series_concat_reduced_rm = misc.rolling_mean_df(series_concat_reduced, elements=120)
            
            # inflow_rl = (series_concat_reduced_rm[self.glof_start:self.model_end] - 
            #             baseflow_series[self.glof_start:self.model_end]).cumsum()[-1] * 60 / 1000 ** 3
            
            # print 'Total volume: {0:.4f} km3'.format(inflow_rl)
            # print 'Ratio: {0:.3f}%'.format(100 * inflow_rl / (self.glof_volume / 1000 ** 3))
             
            return [series_concat_reduced_rm, 1]
        
    def mc_calcs(self, water_vol):
        
        self.water_vol = water_vol
            
        # empty lists for the Monte Carlo simulation results
        self.inflow_list = []
        self.outflow_list = []
        self.baseflow_list = []
            
        self.outflow_max_list = []
        self.outflow_max_time_list = []
        
        if self.mc == 0:
            self.mc_nr = 1
            glof_volumes = [self.water_vol * 1000 ** 3] # convert GLOF volume to m3/sec
    
            self.baseflow_series = pd.Series(self.flow_list_orig, self.date_list, name='flow')
            self.baseflow_series = misc.timeseries_to_minutes(self.baseflow_series, method='linear')
            # make sure the baseflow_series does not start before the glof begins
            if self.baseflow_series.index.min() < self.glof_start:
                raise ValueError('Baseflow series ({}) starts before GLOF ({}), adapt baseflow '\
                                 'accordingly'.format(self.baseflow_series.index.min(), self.glof_start))
            self.baseflow_list = [self.baseflow_series]        
              
        else:
            glof_volumes = [misc.sample_uniform([self.water_vol - self.water_vol * self.glof_volume_var, 
                                                 self.water_vol + self.water_vol * self.glof_volume_var]) 
                                                 * 1000 ** 3 for r in range(0, self.mc_nr)]
            
        mc_range = range(0, self.mc_nr) 
        
        self.mc_counter = 0
            
        for i, self.glof_volume in zip(mc_range, glof_volumes): 
            
            if self.mc == 1:
                
                self.hours_overlap = misc.sample_uniform(self.hours_overlap_range)
    
                # vary the baseflow during the MC simulation
                adapt_value = misc.sample_uniform([1 - self.baseflow_var, 1 + self.baseflow_var])
                
                # adapt the baseflow values except the first one (which is known)            
                for c in range(1, len(self.flow_list)):            
                    
                    self.flow_list[c] = self.flow_list_orig[c] * adapt_value
            
                self.baseflow_series = pd.Series(self.flow_list, self.date_list, name='flow') 
                if self.baseflow_series.index.min() < self.glof_start:
                    raise ValueError('Baseflow series ({}) starts before GLOF ({}), adapt baseflow '\
                                     'accordingly'.format(self.baseflow_series.index.min(), self.glof_start))
                
                # resample to minutes 
                self.baseflow_series = misc.timeseries_to_minutes(self.baseflow_series, method='linear')
                
                self.baseflow_list.append(self.baseflow_series)
    
            fit_start_time = self.fit_end_time - dt.timedelta(hours=self.hours_overlap)
            self.inflow_series_trend_calc = self.inflow_series[fit_start_time:self.fit_end_time]
                
            # call the method that forecasts the inflow into Mendenhall Lake                                             
            [inflow_forecasted, switch] = self.forecast_inflow()
            
            # switch is zero if peak inflow is in the past (which is not plausible)                                     
            if switch == 0:
                raise Exception('Peak inflow is in the past...processing stopped')
                
            # inflow_rl = (inflow_forecasted[self.glof_start:self.model_end] - 
            #             self.baseflow_series[self.glof_start:self.model_end]).cumsum()[-1] * 60 / 1000 ** 3
            
            # print 'Total volume: {0:.4f} km3'.format(inflow_rl)
            # print 'Ratio: {0:.3f}%'.format(100 * inflow_rl / (self.glof_volume / 1000 ** 3))
                    
            # select only the predicted inflow portion
            self.inflow_forecasted = inflow_forecasted[self.fit_end_time:]
                    
            self.inflow_list.append(self.inflow_forecasted)
               
            outflow_forecasted = util.calc_outflow(self.inflow_forecasted, self.series_runoff_rm, 
                                                  lake_area=self.elev_vs_area_mendenhall, 
                                                  runoff_vs_elev_lkup=self.runoff_vs_elev, 
                                                  elev_vs_runoff_lkup=self.elev_vs_runoff) 
                                               
            self.outflow_list.append(outflow_forecasted)
            
            # add the maximum outflow and its time
            self.outflow_max_list.append(outflow_forecasted.max())
            self.outflow_max_time_list.append(outflow_forecasted.idxmax())
            
            # cut baseflow series back to the actual GLOF
            self.baseflow_series_glof = self.baseflow_series[self.glof_start:self.model_end]
                    
            self.mc_counter += 1
            
            if i in range(0, self.mc_nr + 1, 100):
                
                print '{} out of {} done...'.format(i, self.mc_nr)
            
    def post_processing(self):    
        
        # turn interactive plotting off
        plt.ioff()
        
        xstart = 0.02
        ylevel = 0.8
        ydiff = 0.065    
            
        fig, [ax1, ax3] = plt.subplots(2, 1, figsize=(18, 11), facecolor='w')
                    
        ax1.plot_date(self.series_runoff_menden.index, self.series_runoff_menden, '-', 
                      color='red', label='Outflow (measured)', lw=1.7)
        
        # vertical line to show current time
        ax1.plot_date([self.current_time, self.current_time], [10, 900], '-', 
                      color='k', lw=0.5)
        
        ax1.plot_date(self.inflow_series.index, self.inflow_series, '-', color='gray', 
                      label='Inflow (calculated)')
                        
        # mc_range_new accounts for the fact that some of the above iterations are
        # not finished (continue statement)         
        mc_range_new = range(0, self.mc_counter)
        
        percentiles = [0, 50, 100]
        percentiles2 = [5, 50, 95]
         
        # create dataframe with maximum outflow and maximum outflow timing of each curve 
        outflow_max_df = pd.DataFrame({'Outflow_max': self.outflow_max_list, 
                                       'Outflow_max_time': self.outflow_max_time_list})
        
        # sort the maximum outflow values
        try: # old pandas versions
            outflow_max_df_sorted = outflow_max_df.sort('Outflow_max').copy()
        except: # new pandas versions
            outflow_max_df_sorted = outflow_max_df.sort_values(by='Outflow_max').copy()
            
        # loop over percentiles and find the corresponding inflow and outflow curves
        for perc in percentiles2:
    
            # find index of the outflow curve that has the corresponding
            # max runoff percentile        
            pos = int(np.round(np.percentile(mc_range_new, perc), 0))
            index_outflow = outflow_max_df_sorted.iloc[pos].name  
            
            outflow_sel = self.outflow_list[index_outflow]
            inflow_sel = self.inflow_list[index_outflow]
            
            # keep as attribute
            if perc == 5:
                self.outflow_5 = outflow_sel
            if perc == 50:
                self.outflow_50 = outflow_sel
            if perc == 95:
                self.outflow_95 = outflow_sel
            
            ax1.plot_date(outflow_sel.index, outflow_sel, '-', color='k', 
            zorder=100, alpha=1.0, label=('Outflow (modeled, {}th, {}th, '
            '{}th percentile)').format(percentiles2[0], percentiles2[1], percentiles2[2]))
                          
            ax1.plot_date(inflow_sel.index, inflow_sel, ':', color='gray', 
            zorder=-200, alpha=1.0, label=('Inflow (modeled, {}th, {}th, '
            '{}th percentile)').format(percentiles2[0], percentiles2[1], percentiles2[2])) 
        
        # concatenate the outflow list, group by time, and find for each time the 0
        # 50, and 100 percentiles by means of aggregation         
        outflow_all = pd.concat(self.outflow_list)
        outflow_all_stat = outflow_all.groupby(outflow_all.index).agg([misc.percentile(percentiles[0]), 
                                               misc.percentile(percentiles[1]), misc.percentile(percentiles[2])])
                                               
        baseflow_all = pd.concat(self.baseflow_list)
        baseflow_all_stat = baseflow_all.groupby(baseflow_all.index).agg([misc.percentile(percentiles[0]), 
                                               misc.percentile(percentiles[1]), misc.percentile(percentiles[2])])
    
        # plot envelope around all runs (0 and 100 percentile)
        ax1.fill_between(outflow_all_stat.index, eval('outflow_all_stat.percentile_{}'.format(percentiles[0])), 
                         eval('outflow_all_stat.percentile_{}'.format(percentiles[2])), color='orange', 
                         alpha='0.35', zorder=-100)
                         
        ax1.fill_between(baseflow_all_stat.index, eval('baseflow_all_stat.percentile_{}'.format(percentiles[0])), 
                         eval('baseflow_all_stat.percentile_{}'.format(percentiles[2])), color='purple', 
                         alpha='0.1', zorder=-200)
        
        # proxy artist (legend does not work for fill_between)                 
        ax1.fill(np.NaN, np.NaN, color='orange', alpha=0.35, label='Outflow (modeled, envelope around all MC runs)')
        ax1.fill(np.NaN, np.NaN, color='purple', alpha=0.1, label='Inflow baseflow (estimated)')  
        
        # plot the data used for the trend analysis    
        ax1.plot_date(self.inflow_series_trend_calc.index, self.inflow_series_trend_calc, '-', color='limegreen', lw=2, 
                      label='Inflow portion used for trend estimation')  
        
        # annotate GLOF start and end        
        ax1.annotate('GLOF start', xy=(mdates.date2num(self.glof_start), self.inflow_series[self.glof_start]),
                     xycoords='data', xytext=(-50, 30), textcoords='offset points', 
                    arrowprops=dict(arrowstyle='->', connectionstyle='arc3, rad=0.0'), fontsize=12)
                                
        #------------------
        
        if self.log == 1:
            # set y_scale to log, add ticks and
            # use numbers as y_ticklabels rather than scientific notation
            ax1.set_yscale('log')
            ticks = range(0, 60, 10) + range(100, 800, 100)
            ax1.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
            ax1.minorticks_off()
            
        else:
            ticks = range(0, 800, 50)
        
        #------------------
        # annotate the feet levels
        
        ax2 = ax1.twinx()
        
        feet_levels = range(4, 15)
    #    time_covered = maxdate - mindate 
        
        ticks3 = [util.foot_to_runoff(foot, self.elev_vs_runoff) for foot in feet_levels]
        ticks3_labels = [str(foot) for foot in feet_levels]
        
        if self.log == 1:
            ax2.set_yscale('log')
            ax2.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
            ax2.minorticks_off()
            
        ax2.set_yticks(ticks3)
        ax2.set_yticklabels(ticks3_labels)
            
        ax2.set_ylabel('Water level (ft)', size=14, labelpad=10)
        
        for tick3 in ticks3:
            ax1.plot_date([self.fig_maxdate - dt.timedelta(days=35), self.fig_maxdate], [tick3, tick3], 
            '-', color='darkblue', lw=1.0, zorder=-1000, alpha=0.15)
                    
        #------------------
    
        ax1.set_ylabel('Discharge (' + 'm$\mathregular{^3} \!$/s)', size=14, labelpad=10)
        ax1.grid(color='gray', linestyle='-', linewidth=1, alpha=0.1)
        
        ax1.set_xlim(self.fig_mindate, self.fig_maxdate)
        
        ax1.set_yticks(ticks)
        
        if self.log == 1:
            ax1.set_ylim([10, 750])
            ax2.set_ylim([10, 750])
        else:
            ax1.set_ylim([10, 700])
            ax2.set_ylim([10, 700])
        
        misc.annotatefun(ax1, ['Mendenhall Lake'], xstart, 0.9, 0.075, 20)
        
        last_time = self.series_runoff_menden.index[-1].strftime('%Y-%m-%d %H:%M')
        last_level = self.series_runoff_menden[-1]
        
        ylevel = 0.8
        misc.annotatefun(ax1, ['Latest outflow reading'], xstart, ylevel, 0.06, 15)                      
        misc.annotatefun(ax1, [last_time + ': '+ str(round(last_level, 1)) + ' m$\mathregular{^3} \!$/s'], 
                               xstart, ylevel - ydiff, 0.05, 12) 
        
        misc.annotatefun(ax1, ['Source: USGS'], 0.02, 0.07, 0.8, 14)
        
        [handles, labels] = misc.clean_up_legend(ax1)    
        
        ax1.legend(handles, labels, loc='lower left', fontsize=12, bbox_to_anchor=(0.015, 0.285))
        
        misc.format_date_axis(plt, ax1, 'day_month_year', rot=45)
    
        #--------------------------------------------------------------------------    
        
        ln1 = ax3.plot_date(self.series_airtemp_menden.index, self.series_airtemp_menden, 
                            '-', color='red', label='Air temperature')
              
        ax3.set_ylabel('Air temperature ({})'.format(misc.symbols_ck('degreeC')), 
                       size=14, labelpad=10)
        ax3.grid(color='gray', linestyle='-', linewidth=1, alpha=0.1)
        
        ax3.plot_date([self.current_time, self.current_time], [0, 50], '-', color='k', lw=0.5)
        
        misc.minorlabeling(ax3, np.arange(0, 30, 1), 'y')
        ax3.set_ylim([0, 30])
        
        misc.annotatefun(ax3, ['Mendenhall Lake'], xstart, 0.9, 0.075, 20)
         
        misc.annotatefun(ax3, ['Source: USGS'], 0.91, 0.05, 0.8, 14)
        
        misc.format_date_axis(plt, ax3, 'day_month_year', rot=45)
        
        #--------------------------------------------------------------------------
        # create hourly sums for precip   
        hourly_precip_series = self.series_precip_menden.groupby(pd.Grouper(freq='H')).agg('sum')
        
        ax4 = ax3.twinx()
        
        ln2 = ax4.plot_date(hourly_precip_series.index, hourly_precip_series, '-', 
                            color='darkblue', label='Hourly precipitation')
        
        ax4.set_ylabel('Hourly precipitation ({})'.format('mm'), size=14, labelpad=10)
        
        ax4.set_ylim([0, 30])
        misc.minorlabeling(ax4, np.arange(0, 30, 1), 'y') 
        ax4.set_xlim(self.fig_mindate, self.fig_maxdate)
        
        lns = ln1 + ln2
        labs = [l.get_label() for l in lns]
        ax4.legend(lns, labs, loc='upper right', fontsize=12)
    
        misc.format_date_axis(plt, ax4, 'day_month_year', rot=45)
        
        hours = matplotlib.dates.HourLocator() 
        hours.MAXTICKS = 5000
        ax1.xaxis.set_minor_locator(hours)
        ax3.xaxis.set_minor_locator(hours)
        ax4.xaxis.set_minor_locator(hours)    
        
        #--------------------------------------------------------------------------
        
        fig.tight_layout()
            
        fig.savefig('./plots/SuiB_scenario_automatic.png', dpi=200)
        
        #fig.savefig(r'C:\Users\Ch.Kienholz\Google Drive\SuiB\SuiB_scenario_automatic.png', dpi=200)
          
        plt.close()    
    
#%%
    
class Mendenhall_Lake_GLOF_Model_Parameters_OBS(object):
    '''
    Class that provides the input parameters for Mendenhall_Lake_GLOF_Model_OBS.
    Adapt parameters as needed.
    testmode = 1 allows for script testing, using 2019 data as input.
    '''
    def __init__(self):

        testmode = 1 # (1) load provided 2019 data to test the script, (0) download current data from usgs server

        # time when the inflow curve starts rising
        self.glof_start = pd.to_datetime('2020-07-12 23:30') 
        
        # (1) load baseflow data from textfile, (0) automatically create linear baseflow curve
        self.baseflow_text = 1  
        self.baseflow_text_path = './data/test_2019/baseflow_2019.csv' # only needed in case baseflow_text == 1

        # define the time limits for the final figure       
        self.fig_mindate = pd.to_datetime('2019-07-13 18:00') - dt.timedelta(days=5)
        self.fig_maxdate = pd.to_datetime('2019-07-13 18:00') + dt.timedelta(days=5)         
        
        # create instance to download gauge and weather data from the usgs server       
        Mendenhall_series = Mendenhall_Lake_Gauge_Weather()
        
        if testmode == 0:
            Mendenhall_series.download(days=10) # download data from the last 10 days
        
        if testmode == 1: # load pre-downloaded series for script testing 
            Mendenhall_series.series_runoff = pd.read_pickle('./data/test_2019/series_runoff_menden_2019.pickle')
            Mendenhall_series.series_gage_height = pd.read_pickle('./data/test_2019/series_gage_height_menden_2019.pickle')
            Mendenhall_series.series_airtemp = pd.read_pickle('./data/test_2019/series_airtemp_menden_2019.pickle')
            Mendenhall_series.series_precip = pd.read_pickle('./data/test_2019/series_precip_menden_2019.pickle')

            self.glof_start = pd.to_datetime('2019-07-12 23:30')
            self.baseflow_text = 1
            self.baseflow_text_path = './data/test_2019/baseflow_2019.csv'  

            # define the time limits for the final figure       
            self.fig_mindate = pd.to_datetime('2019-07-13 18:00') - dt.timedelta(days=5)
            self.fig_maxdate = pd.to_datetime('2019-07-13 18:00') + dt.timedelta(days=5)                   
                                          
        self.series_runoff_menden = Mendenhall_series.series_runoff  
        self.series_gage_height_menden = Mendenhall_series.series_gage_height 
        self.series_airtemp_menden = Mendenhall_series.series_airtemp
        self.series_precip_menden = Mendenhall_series.series_precip 

        self.mc = 1 # switch for monte carlo simulations yes (1) or no (0)
        
        if self.mc == 1:
            # how many monte carlo simulations to run
            self.mc_nr = 20
            # by how much to vary the glof volume (0.1 is +-10%)
            self.glof_volume_var = 0.1
            # by how much to vary the baseflow (0.3 is +-30%)
            self.baseflow_var = 0.3
            # by how much to vary the water volume percentage drained 
            # during the rising limb (0.05 is +-5%)
            self.rising_var = 0.05
            
        else:
            self.mc_nr = 1
        
        # for how long to run the model
        self.model_end = self.glof_start + dt.timedelta(days=6) 
            
        # load lookup tables to convert between discharge and Mendenhall Lake 
        # elevation and vice versa
        self.runoff_vs_elev = pd.read_pickle('./data/runoff_vs_elev.pickle')
        self.elev_vs_runoff = pd.read_pickle('./data/elev_vs_runoff.pickle')
    
        # load lookup table that contains the Mendenhall Lake areas as a function of the water level     
        self.elev_vs_area_mendenhall = pd.read_pickle('./data/mendenhall_area_lookup.pickle')
    
        # load file with the observed GLOF inflow curves (inflow into Mendenhall Lake) 
        self.inflow_curve_obs_all = pd.read_pickle('./data/suicide_inflow_historic.pickle')
    
        # load the file that specifies how much of the water drains during the rising limb
        self.percentage_file = pd.read_pickle('./data/percentage_rising_limb.pickle')    
    
class Mendenhall_Lake_GLOF_Model_OBS(Mendenhall_Lake_GLOF_Model_Parameters_OBS):
    '''
    Class predicting the water inflow into Mendenhall Lake for a given GLOF volume 
    and baseflow. The shape of the inflow curve is taken from observed inflow 
    curves from previous years. Several parameters are varied as part of the MC calculations.
    The outflow from Mendenhall Lake is calculated from the inflow via storage function.
    '''
    def __init__(self):
        
        # get the model parameters from the parameter class
        Mendenhall_Lake_GLOF_Model_Parameters_OBS.__init__(self)

        # rolling mean, resample to minutes, and linear interpolation for nans
        series_runoff_rm = misc.rolling_mean_df(self.series_runoff_menden, elements=15)
        self.series_runoff_rm = misc.timeseries_to_minutes(series_runoff_rm, method='linear')
        
        # rolling mean, resample to minutes, and linear interpolation for nans
        series_gage_height_rm = misc.rolling_mean_df(self.series_gage_height_menden, elements=15)
        series_gage_height_rm = misc.timeseries_to_minutes(series_gage_height_rm, method='linear')
        
        # call the calc_inflow function using the smoothed discharge and gauge height series
        inflow_series = util.calc_inflow(self.series_runoff_rm, series_gage_height_rm, 
                                         lake_area=self.elev_vs_area_mendenhall, smoothing=1, 
                                         smoothing_len=120)
        
        # cut the inflow series at the glof_start date 
        self.inflow_series = inflow_series[:self.glof_start]
        
        #----------------------
        # code to create a baseflow series
        # set baseflow value at the glof_start time
        # to the value of the inflow_series at the glof_start time
        self.base_inflow_val = inflow_series[self.glof_start]

        if self.baseflow_text == 1:
            # load textfile with predicted baseflow (e.g. passed from NOAA model)
            bsf = pd.read_csv(self.baseflow_text_path)
            # set the baseflow value at glof start to the value of the inflow
            date_list = [self.glof_start] + list(bsf['date']) 
            self.flow_list = [self.base_inflow_val] + list(bsf['flow'])         
        
        else:    
            # create a constant baseflow curve from scratch
            # additional values can be added between first and last value if needed
            # working with an actual text file is recommended for this task
            date_list = [self.glof_start, self.model_end] # '2019-07-16 00:00', '2019-07-18 12:00', self.model_end
            self.flow_list = [self.base_inflow_val, self.base_inflow_val] # , 70, 85, self.base_inflow_val
                                    
        # convert date strings to datetimes
        self.date_list = [pd.to_datetime(date) for date in date_list] 
            
        # create a copy of the original baseflow list (to be used in the MC simulations)
        self.flow_list_orig = np.array(self.flow_list).copy()
        
    def forecast_inflow(self): 
        '''
        produces an inflow prediction for Mendenhall Lake using an observed 
        inflow curve as a template the template for the observed inflow does 
        not include baseflow.
        '''
                
        # convert to datetimes and create new series with datetime index
        time_index = misc.delta_hours_to_time(self.inflow_curve_obs.index, start_time=self.glof_start)      
        series_extended = pd.Series(self.inflow_curve_obs.get_values(), index=time_index)
        
        # sum historic inflow and baseflow
        series_extended = series_extended + self.baseflow_series
        series_extended.name = 'discharge'
            
        # check if the extended inflow series or the baseflow series is longer
        # if baseflow series is longer, cut it to the exent of the extended inflow series 
        if series_extended.index[-1] < self.baseflow_series.index[-1]:
            
            baseflow_series = self.baseflow_series[:series_extended.index[-1]] 
        
        # if the extended inflow series is longer, extend baseflow series by adding the baseflow end val     
        else:
            
            baseflow_series_extension = series_extended[self.baseflow_series.index[-1]:][1:]
            baseflow_series_extension[:] = self.baseflow_series[-1] # allocate baseflow_end_val 
            baseflow_series = pd.concat([self.baseflow_series, baseflow_series_extension], axis=0)
            
        # create runoff cumsum and subtract cumsum of base runoff    
        series_extended_cumsum = series_extended.cumsum() - baseflow_series.cumsum()
        series_extended_cumsum.name = 'discharge_cumsum'
                 
        # create dataframe with discharge and cumsum discharge
        series_concat = pd.concat([series_extended, series_extended_cumsum], axis=1)
        
        # allocate a certain percentage of the GLOF volume to the rising limb     
        if self.mc == 0:
            percentage_rising = self.percentage_rising / 100.0
        else: # vary this percentage in case the monte carlo simulation option is turned on
            percentage_rising = misc.sample_uniform([(self.percentage_rising / 100.0) - self.rising_var, 
                                              (self.percentage_rising / 100.0) + self.rising_var])
      
        glof_volume_up = percentage_rising * self.glof_volume
    
        # identify peak discharge and time of peak discharge
        # multiply with 60 to account for the fact that the series contain measurements 
        # every minute rather than every second
        peak_discharge = series_concat.loc[((series_concat['discharge_cumsum']) * 
                                           60 <= glof_volume_up), 'discharge'][-1]
        glof_volume_up_rl = series_concat.loc[((series_concat['discharge_cumsum']) * 
                                              60 <= glof_volume_up), 'discharge_cumsum'][-1] * 60
        peak_time = series_concat.loc[((series_concat['discharge_cumsum']) * 
                                      60 <= glof_volume_up)].index[-1]
         
        glof_volume_down = self.glof_volume - glof_volume_up_rl 
            
        #------------------------------------------------------------------------------------------------
        # the following calculations assume a linear decrease of flow between the peak flow and base flow
        # in order to conserve water volume, the slope of this curve needs to be determined,
        # using trigonometry (the area of the triangle must match the glof volume)
        # if the baseflow is constant, no iterative approach is required
        # if the baseflow is not constant, an iterative approach is required because
        # there are too many unknowns (slope of falling limb affects the amount of baseflow)
    
        # difference between peak flow and base flow
        # for first iteration, assume the baseflow is constant at level of peak time
        baseflow_mean_prior = baseflow_series[peak_time]
        flow_diff = peak_discharge - baseflow_mean_prior
    
        # assume linear decrease of flow with a 90 degree angle
        minutes_remaining = round((glof_volume_down * 2.0) / (flow_diff * 60), 0)
        
        # large volume difference to start
        volume_difference = 10 ** 10
        volume_diff_prev = volume_difference + 10
        n = 0         
        
        while n < 100:  # allow up to 100 iterations
                   
            # determine intersection time
            time_intersect = peak_time + dt.timedelta(minutes=minutes_remaining)
            
            # retain only the series portions before peak time and after intersection time
            series_concat_sel = series_concat.loc[(series_concat.index <= peak_time)
                                                  | (series_concat.index >= time_intersect)]['discharge'] 

            # set series after the intersection time to the baseflow value
            series_concat_sel[time_intersect:] = baseflow_series[time_intersect:]
            
            # interpolate to minutes, which introduces a linear drop between peak and intersection time
            series_concat_sel = misc.timeseries_to_minutes(series_concat_sel, method='linear')
            
            # calculate the volume difference between the target glof_volume_down
            # and the actual glof_volume_down
            # multiply by 60 to account for the fact that we work on a time series
            # at minute spatial resolution
            volume_difference = glof_volume_down - 60 *(series_concat_sel[peak_time:time_intersect].cumsum()[-1] 
                                                        - baseflow_series[peak_time:time_intersect].cumsum()[-1])

            # print minutes_remaining
            # print volume_difference            
            
            # adapt in one minute steps
            if volume_difference > 0:
                minutes_remaining = minutes_remaining + 1
                
            else:
                minutes_remaining = minutes_remaining - 1
            
            # check whether the volume difference improves
            if abs(volume_difference) - volume_diff_prev >= 0:
                # print 'no improvement... stop iteration'
                time_intersect = time_intersect_prev
                break
                
            volume_diff_prev = abs(volume_difference)
            time_intersect_prev = time_intersect
                
            n += 1
        
        time_intersect = peak_time + dt.timedelta(minutes=minutes_remaining)
        
        # retain only the series portions before peak time and after intersection time
        series_concat = series_concat.loc[(series_concat.index <= peak_time) 
                                          | (series_concat.index >= time_intersect)]['discharge'] 
               
        # set series after the intersection time to the baseflow values
        series_concat[time_intersect:] = baseflow_series[time_intersect:]
         
        # cut series to predefined length (5 days after intersection time)
        time_limit = time_intersect + dt.timedelta(days=5)
        series_concat_reduced = series_concat[:time_limit]
        
        # resample to minutes (the linear falling limb does not yet have minute intervals)
        series_concat_reduced = misc.timeseries_to_minutes(series_concat_reduced, method='linear')
        
        # rolling mean over 1 hour
        series_concat_reduced_rm = misc.rolling_mean_df(series_concat_reduced, elements=60)
        
        return series_concat_reduced_rm
        
    def mc_calcs(self, water_vol, scenario_year):
                        
        self.water_vol = water_vol
        self.scenario_year = scenario_year
        
        # from the observed GLOF inflow curves (inflow into Mendenhall Lake)
        # select the one for the year of interest  
        self.inflow_curve_obs = self.inflow_curve_obs_all[scenario_year] 
        # select the percentage value for the year of interest               
        self.percentage_rising = self.percentage_file[scenario_year] 
                                   
        # empty lists for the MC simulation results
        self.inflow_list = []
        self.outflow_list = []
        self.baseflow_list = []
            
        self.outflow_max_list = []
        self.outflow_max_time_list = []
        
        if self.mc == 0:
            self.mc_nr = 1
            glof_volumes = [self.water_vol * 1000 ** 3] # convert to m3
            self.baseflow_series = pd.Series(self.flow_list_orig, self.date_list, name='flow')
            self.baseflow_series = misc.timeseries_to_minutes(self.baseflow_series, method='linear')
            
            # make sure the baseflow_series does not start before the glof begins
            if self.baseflow_series.index.min() < self.glof_start:
                raise ValueError('Baseflow series ({}) starts before GLOF ({}), adapt baseflow '\
                                 'accordingly'.format(self.baseflow_series.index.min(), self.glof_start))
            self.baseflow_list = [self.baseflow_series] 
             
        else:
            glof_volumes = [misc.sample_uniform([self.water_vol - self.water_vol * self.glof_volume_var, 
                                                 self.water_vol + self.water_vol * self.glof_volume_var])  
                                                * 1000 ** 3 for r in range(0, self.mc_nr)]
            
        mc_range = range(0, self.mc_nr) 
        self.mc_counter = 0
           
        for i, self.glof_volume in zip(mc_range, glof_volumes): 
                    
            if self.mc == 1:
    
                # vary the baseflow during the MC simulation
                adapt_value = misc.sample_uniform([1 - self.baseflow_var, 1 + self.baseflow_var])
                
                # adapt the baseflow values except the first one (which is known)            
                for c in range(1, len(self.flow_list)):            
                    
                    self.flow_list[c] = self.flow_list_orig[c] * adapt_value
            
                self.baseflow_series = pd.Series(self.flow_list, self.date_list, name='flow') 
                if self.baseflow_series.index.min() < self.glof_start:
                    raise ValueError('Baseflow series ({}) starts before GLOF ({}), adapt baseflow '\
                                     'accordingly'.format(self.baseflow_series.index.min(), self.glof_start))
                
                # resample to minutes 
                self.baseflow_series = misc.timeseries_to_minutes(self.baseflow_series, method='linear')
                
                self.baseflow_list.append(self.baseflow_series)
            
            # call the method that forecasts the inflow into Mendenhall Lake 
            self.inflow_forecasted = self.forecast_inflow()
            
            self.inflow_list.append(self.inflow_forecasted)
            
            # calculate the corresponding lake outflow
            outflow_forecasted = util.calc_outflow(self.inflow_forecasted, self.series_runoff_rm, 
                                                  lake_area=self.elev_vs_area_mendenhall, 
                                                  runoff_vs_elev_lkup=self.runoff_vs_elev, 
                                                  elev_vs_runoff_lkup=self.elev_vs_runoff) 
                                               
            self.outflow_list.append(outflow_forecasted)
            
            inflow_rl = (self.inflow_forecasted[self.glof_start:self.model_end] - 
                        self.baseflow_series[self.glof_start:self.model_end]).cumsum()[-1] * 60 / 1000 ** 3
            
            print 'Total volume: {0:.4f} km3'.format(inflow_rl)
            #print 'Ratio: {0:.3f}%'.format(100 * inflow_rl / (self.glof_volume / 1000 ** 3))
                         
            # add the maximum outflow and its time
            self.outflow_max_list.append(outflow_forecasted.max())
            self.outflow_max_time_list.append(outflow_forecasted.idxmax())
                        
            self.mc_counter += 1
                
            if i in range(0, self.mc_nr + 1, 100):
                
                print '{} out of {} done...'.format(i, self.mc_nr)
            
    def post_processing(self):

        # turn interactive plotting off
        plt.ioff() 
                    
        # create figure with two subpanels for plotting      
        fig, [ax1, ax3] = plt.subplots(2, 1, figsize=(18, 11), facecolor='w')
                
        # plot the outflow curve at Mendenhall Lake     
        ax1.plot_date(self.series_runoff_menden.index, self.series_runoff_menden, '-', 
                      color='red', label='Outflow (measured)', lw=1.7)            
        
        ax1.plot_date(self.inflow_series.index, self.inflow_series, '-', color='gray', 
                      label='Inflow (calculated)')    
                      
        percentiles = [0, 50, 100]
        percentiles2 = [5, 50, 95]
         
        # create dataframe with maximum outflow and maximum outflow timing of each curve 
        outflow_max_df = pd.DataFrame({'Outflow_max': self.outflow_max_list, 
                                       'Outflow_max_time': self.outflow_max_time_list})
        
        # sort the maximum outflow values
        try: # old pandas versions    
            outflow_max_df_sorted = outflow_max_df.sort('Outflow_max').copy()
        except: # new pandas versions   
            outflow_max_df_sorted = outflow_max_df.sort_values(by='Outflow_max').copy()
        
        # loop over percentiles and find the corresponding inflow and outflow curves
        for perc in percentiles2:
    
            # find index of the outflow curve that has the corresponding
            # max runoff percentile        
            pos = int(np.round(np.percentile(range(0, self.mc_counter), perc), 0))
            index_outflow = outflow_max_df_sorted.iloc[pos].name  
            
            outflow_sel = self.outflow_list[index_outflow]
            outflow_sel.name = '{}-type'.format(self.scenario_year)
            inflow_sel = self.inflow_list[index_outflow]
            
            # keep as attribute
            if perc == 5:
                self.outflow_5 = outflow_sel
            if perc == 50:
                self.outflow_50 = outflow_sel
            if perc == 95:
                self.outflow_95 = outflow_sel
                
            ax1.plot_date(outflow_sel.index, outflow_sel, '-', color='k', 
            zorder=100, alpha=1.0, label=('Outflow (modeled, {}th, {}th, '
            '{}th percentile)').format(percentiles2[0], percentiles2[1], percentiles2[2]))
                          
            ax1.plot_date(inflow_sel.index, inflow_sel, ':', color='gray', 
            zorder=-200, alpha=1.0, label=('Inflow (modeled, {}th, {}th,'
            ' {}th percentile)').format(percentiles2[0], percentiles2[1], percentiles2[2])) 
        
        # concatenate the outflow list, group by time, and find for each time the 0
        # 50, and 100 percentiles by means of aggregation        
        outflow_all = pd.concat(self.outflow_list)
        outflow_all_stat = outflow_all.groupby(outflow_all.index).agg([misc.percentile(percentiles[0]), 
                                               misc.percentile(percentiles[1]), misc.percentile(percentiles[2])])
                                                   
        baseflow_all = pd.concat(self.baseflow_list)
        baseflow_all_stat = baseflow_all.groupby(baseflow_all.index).agg([misc.percentile(percentiles[0]), 
                                               misc.percentile(percentiles[1]), misc.percentile(percentiles[2])])
    
        # plot envelope around all runs (0 and 100 percentile)
        ax1.fill_between(outflow_all_stat.index, eval('outflow_all_stat.percentile_{}'.format(percentiles[0])), 
                         eval('outflow_all_stat.percentile_{}'.format(percentiles[2])), color='orange', 
                         alpha='0.35', zorder=-100) 
            
        # plot envelope around all runs (0 and 100 percentile)
        ax1.fill_between(baseflow_all_stat.index, eval('baseflow_all_stat.percentile_{}'.format(percentiles[0])), 
                         eval('baseflow_all_stat.percentile_{}'.format(percentiles[2])), color='purple', 
                         alpha='0.1', zorder=-200) 
                         
        # proxy artist (legend does not work for fill_between)                 
        ax1.fill(np.NaN, np.NaN, color='orange', alpha=0.35, label='Outflow (modeled, envelope around all MC runs)')
        ax1.fill(np.NaN, np.NaN, color='purple', alpha=0.1, label='Inflow baseflow (estimated)')                     
        
        # annotate GLOF start       
        ax1.annotate('GLOF start', xy=(mdates.date2num(self.glof_start), self.inflow_series[self.glof_start]),
                     xycoords='data', xytext=(-50, 30), textcoords='offset points', 
                    arrowprops=dict(arrowstyle='->', connectionstyle='arc3, rad=0'), fontsize=12)
                        
        #------------------
        # annotate the foot levels    
        ax2 = ax1.twinx()
        
        feet_levels = range(4, 15)
        
        ticks3 = [util.foot_to_runoff(foot, self.elev_vs_runoff) for foot in feet_levels]
        ticks3_labels = [str(foot) for foot in feet_levels]
                
        ax2.set_yticks(ticks3)
        ax2.set_yticklabels(ticks3_labels)
            
        ax2.set_ylabel('Water level (ft)', size=14, labelpad=10)
        
        for tick3 in ticks3:
            ax1.plot_date([self.fig_maxdate - dt.timedelta(days=4), self.fig_maxdate], [tick3, tick3], 
            '-', color='darkblue', lw=1.0, zorder=-1000, alpha=0.2)
            
        ax1.set_ylabel('Discharge (' + 'm$\mathregular{^3} \!$/s)', size=14, labelpad=10)
        ax1.grid(color='gray', linestyle='-', linewidth=1, alpha=0.1)
        
        ax1.set_xlim(self.fig_mindate, self.fig_maxdate)
        
        ticks = range(0, 800, 50)
        ax1.set_yticks(ticks)
        
        ax1.set_ylim([10, 700])
        ax2.set_ylim([10, 700])
        
        misc.annotatefun(ax1, ['Mendenhall Lake'], 0.02, 0.9, 0.1, 20)
                                  
        misc.annotatefun(ax1, ['{}-style event'.format(self.scenario_year), 
                              'Total volume drained: ' + str(self.water_vol) + ' km$\mathregular{^3} \mathregular{\pm}$' + ' 10%',
                              'Volume drained during rising inflow limb: {} '.format(self.percentage_rising) + '$\mathregular{\pm}$' + ' 5%', 
                              'GLOF start time: {}'.format(self.glof_start.strftime('%Y-%m-%d %H:%M')), 
                              'Start baseflow: {0:.0f}'.format(self.base_inflow_val) + ' m$\mathregular{^3}$/s'], 
                               0.02, 0.8, 0.08, 14)
        
        misc.annotatefun(ax1, ['Source: USGS'], 0.02, 0.07, 0.8, 14)
        
        [handles, labels] = misc.clean_up_legend(ax1)    
        
        ax1.legend(handles, labels, loc='lower left', fontsize=12, 
                   bbox_to_anchor=(0.325, 0.55))
        
        misc.format_date_axis(plt, ax1, 'day_month_year', rot=45)
        
        #--------------------------------------------------------------------------    
        
        ln1 = ax3.plot_date(self.series_airtemp_menden.index, self.series_airtemp_menden, '-', 
                            color='red', label='Air temperature')
              
        ax3.set_ylabel('Air temperature ({})'.format(misc.symbols_ck('degreeC')), 
                       size=14, labelpad=10)
        ax3.grid(color='gray', linestyle='-', linewidth=1, alpha=0.1)
        
        misc.minorlabeling(ax3, np.arange(0, 30, 1), 'y')
         
        ax3.set_ylim([0, 30])
        
        misc.annotatefun(ax3, ['Mendenhall Lake'], 0.02, 0.9, 0.075, 20)
        misc.annotatefun(ax3, ['Source: USGS'], 0.91, 0.05, 0.8, 14)
            
        #--------------------------------------------------------------------------
        # create hourly sums for precipitation   
        hourly_precip_series = self.series_precip_menden.groupby(pd.Grouper(freq='H')).agg('sum')
        
        ax4 = ax3.twinx()
        
        ln2 = ax4.plot_date(hourly_precip_series.index, hourly_precip_series, '-', 
                            color='darkblue', label='Hourly precipitation')
        
        ax4.set_ylabel('Hourly precipitation ({})'.format('mm'), size=14, labelpad=10)
        
        ax4.set_ylim([0, 30])
        misc.minorlabeling(ax4, np.arange(0, 30, 1), 'y') 
        ax4.set_xlim(self.fig_mindate, self.fig_maxdate)
        
        lns = ln1 + ln2
        labs = [l.get_label() for l in lns]
        ax4.legend(lns, labs, loc='upper right', fontsize=12)
        
        misc.format_date_axis(plt, ax3, 'day_month_year', rot=45)
    
        hours = matplotlib.dates.HourLocator() 
        hours.MAXTICKS = 5000
        ax1.xaxis.set_minor_locator(hours)
        ax3.xaxis.set_minor_locator(hours)
        ax4.xaxis.set_minor_locator(hours)
    
        plt.tight_layout()
            
        fig.savefig('./plots/SuiB_scenario_{}.png'.format(self.scenario_year), dpi=200)
        
        plt.close()
 
#%%   
     
def main():
    
    # calculate water volume estimate for the upcoming GLOF event, based on 
    # pre-GLOF water levels (measured from gauge and camera) and post-GLOF 
    # water levels (estimated using data from previous events)
    water_vol = util.calc_suicide_volume_2019(start_height=436, end_height=394)
        
    # create the forecaster object for the observation-based model
    # this loads the parameters defined in the class 
    # "Mendenhall_Lake_GLOF_Model_Parameters_OBS" (check and adapt if needed) 
    forecaster = Mendenhall_Lake_GLOF_Model_OBS()
    
    # list to collect the median ouflow for each scenario year 
    # medianlist = []
          
    for scenario_year in [2011, 2012, 2014, 2016, 2018]:            

        # run the calculations for each scenario year
        # this creates 2011-style events, 2012-style events                                                          
        forecaster.mc_calcs(water_vol, scenario_year)
        
        # post_process and plot the results (one figure per year)        
        forecaster.post_processing()
        
        # append the median outflow
        # medianlist.append(forecaster.outflow_50)
    
    # concatenate and plot the median outflows for each scenario year    
    # tmp = pd.concat(medianlist, 1)
    # tmp.plot()
    # tmp.to_csv('./data/median_scenarios_test.csv')
        
    #--------------------------------------------------------
    # create the forecaster object for the real-time forecasting
    # this loads the parameters defined in the class 
    # "Mendenhall_Lake_GLOF_Model_Parameters" (check and adapt if needed)  
    forecaster_automatic = Mendenhall_Lake_GLOF_Model()
     
    # run the mc calculations                                       
    forecaster_automatic.mc_calcs(water_vol)
    
    # post_process and plot the results        
    forecaster_automatic.post_processing()
    
    # retrieval of outflow timeseries
    # tmp = forecaster_automatic.outflow_list

if __name__ == "__main__":
   
    main()
