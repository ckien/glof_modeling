# glof_modeling

### About

Statistical models to predict GLOF  peak discharge and timing along Mendenhall Lake and River, following the lake release at Suicide Basin.

The code allows to estimate:

- the pre-GLOF water volume in the basin
- timing and magnitude of the peak inflow into Mendenhall Lake
- timing and magnitude of the peak flow at Mendenhall River

The main model *Mendenhall_Lake_GLOF_Model*  provides the most likely GLOF evolution. This model is designed to be run continuously to provide updates in real-time. A Monte Carlo approach is chosen to deal with the uncertainties involved. Uncertainty ranges are derived from analyzing monitored events. 

The model *Mendenhall_Lake_GLOF_Model_OBS* mimics individual events from previous years to provide 2012-style, 2013-style predictions.  

Written and tested in Python 2.7. Key dependencies: numpy, pandas, scipy. 




### Example 1 ###

```python
    # calculate water volume estimate for the upcoming GLOF event, based on 
    # pre-GLOF water levels (measured from gauge and camera) and post-GLOF 
    # water levels (estimated using data from previous events)
    water_vol = util.calc_suicide_volume_2019(start_height=436, end_height=394)
        
    # create the forecaster object for the observation-based model
    # this loads the parameters defined in the class 
    # "Mendenhall_Lake_GLOF_Model_Parameters_OBS" (check and adapt if needed) 
    forecaster = Mendenhall_Lake_GLOF_Model_OBS()
    
    # list to collect the median lake ouflow for each scenario year 
    medianlist = []
          
    for scenario_year in [2011, 2012, 2014, 2016, 2018]:            

        # run the calculations for each scenario year
        # creating 2011-style events, 2012-style events, etc.      
        forecaster.mc_calcs(water_vol, scenario_year)
        
        # post_process and plot the results (one figure per year)        
        forecaster.post_processing()
        
        # append the median outflow
        medianlist.append(forecaster.outflow_50)
    
    # concatenate and plot the median outflows for each scenario year    
    tmp = pd.concat(medianlist, 1)
    tmp.plot()
    tmp.to_csv('./data/median_scenarios_test.csv')
```

### Example 2

```python
    # create the forecaster object for the real-time forecasting
    # this loads the parameters defined in the class 
    # "Mendenhall_Lake_GLOF_Model_Parameters" (check and adapt if needed)  
    forecaster_automatic = Mendenhall_Lake_GLOF_Model()
     
    # run the mc calculations                                       
    forecaster_automatic.mc_calcs(water_vol)
    
    # post_process and plot the results        
    forecaster_automatic.post_processing()  
       
```

