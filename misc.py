# -*- coding: utf-8 -*-
"""
Created on Mon Aug 12 2019

@author: Ch.Kienholz
"""

import pandas as pd
import datetime as dt

import numpy as np

from collections import OrderedDict

#%% Dataframe and stats-related functions
    
def sample_uniform(inputlist):
    [lowerlimit, upperlimit] = inputlist
    return float((np.random.uniform(lowerlimit, upperlimit, 1)))
    
def sample_gaussian(inputlist):
    [mu, sigma] = inputlist
    num = np.random.standard_normal(1)
    return float((num*sigma + mu))
            
def percentile(n):
    def percentile_(x):
        return np.percentile(x, n)
    percentile_.__name__ = 'percentile_%s' % n
    return percentile_ 
    
def timeseries_to_minutes(series, method='linear', order=3):
    '''
    resample timeseries to minutes and linearly interpolate for nans
    '''
    series = series.resample('T')
    if method <> 'polynomial':
        series = series.interpolate(method=method)
    else:
        series = series.interpolate(method=method, order=order)                
    return series
    
def time_to_delta_hours(series):
    '''
    convert datetime into delta hours since start time.
    scipy curve fit does not work with times, hence conversion is needed
    '''
    
    delta_time = (series.index - series.index[0]).tolist() 
    delta_hours = np.array([t.total_seconds() for t in delta_time]) /  (60.0 ** 2)
    
    return delta_hours 
    
def delta_hours_to_time(delta_hours, start_time):
    '''
    convert delta hours since start time back into datetimes
    '''
    
    timeobject = [start_time + dt.timedelta(hours=hour) for hour in list(delta_hours)]
    
    return timeobject
    
def rolling_mean_df(df, elements=15):
    
    try: # old pandas versions

        # take rolling mean to smooth out curves, rolling mean over 15 elements
        df_rm = pd.rolling_mean(df, elements, center=1)
    
        # replace the nans at the end of the series with rolling means over fewer elements
        for loc in range(1, (elements / 2) + 1):
            loc2 = 2 * loc - 1       
            df_rm.iloc[-loc] = pd.rolling_mean(df[-loc2:], loc2, center=1).iloc[-loc]
            
        # replace the nans at the beginning of the series with rolling means over fewer elements
        for loc in range(0, (elements / 2)):
            loc2 = 2 * loc + 1 
            df_rm.iloc[loc] = pd.rolling_mean(df[0:loc2 + 1], loc2, center=1).iloc[loc]
            
    except: # new pandas versions

        # take rolling mean to smooth out curves, rolling mean over 15 elements        
        df_rm = df.rolling(elements, center=True).mean()
    
        # replace the nans at the end of the series with rolling means over fewer elements
        for loc in range(1, (elements / 2) + 1):
            loc2 = 2 * loc - 1       
            df_rm.iloc[-loc] = df[-loc2:].rolling(loc2, center=True).mean().iloc[-loc]
            
        # replace the nans at the beginning of the series with rolling means over fewer elements
        for loc in range(0, (elements / 2)):
            loc2 = 2 * loc + 1 
            df_rm.iloc[loc] = df[0:loc2 + 1].rolling(loc2, center=True).mean().iloc[loc]        
          
    return df_rm
    
#%% Plotting-related functions

def clean_up_legend(ax):
    handles, labels = ax.get_legend_handles_labels()
    # remove duplicate lables and handles
    by_label = OrderedDict(zip(labels, handles))
    # sort both labels and handles by labels
    labels, handles = zip(*sorted(zip(by_label.keys(), by_label.values()), key=lambda t: t[0]))
    
    return [handles, labels]    
    
def format_date_axis(plt, ax, kw, rot=45, yr=10, sbyr=1):
    from matplotlib.dates import YearLocator
    from matplotlib.dates import MonthLocator
    from matplotlib.dates import DayLocator
    from matplotlib.dates import WeekdayLocator
    from matplotlib.dates import DateFormatter
    from matplotlib.dates import MO
        
    if kw == 'tenyear_year':
        years10 = YearLocator(yr)
        years = YearLocator(sbyr)
        Fmt = DateFormatter('%Y')
    
        # format the ticks
        ax.xaxis.set_major_locator(years10)
        ax.xaxis.set_major_formatter(Fmt)
        ax.xaxis.set_minor_locator(years)
        
    if  kw == 'months':
        years = YearLocator(1)
        months = MonthLocator()
        Fmt = DateFormatter('%m/%y')
        ax.xaxis.set_major_locator(months)
        ax.xaxis.set_major_formatter(Fmt)
        
    if  kw == 'weeks':
        weeks = WeekdayLocator(byweekday = MO, interval=1)
        days = DayLocator() # every month
        Fmt = DateFormatter('%m/%d')
        ax.xaxis.set_major_locator(weeks)
        ax.xaxis.set_major_formatter(Fmt)
        ax.xaxis.set_minor_locator(days)
        
    if  kw == 'days':
        years = YearLocator(1)
        months = MonthLocator()
        days = DayLocator()
        Fmt = DateFormatter('%m/%d')
        ax.xaxis.set_major_locator(days)
        ax.xaxis.set_major_formatter(Fmt)
        
    if  kw == 'day_month_year':
        years = YearLocator(1)
        months = MonthLocator()
        days = DayLocator()
        Fmt = DateFormatter('%m/%d/%y')
        ax.xaxis.set_major_locator(days)
        ax.xaxis.set_major_formatter(Fmt)
        
    labels = ax.get_xticklabels()
    plt.setp(labels, rotation=rot)  

def minorlabeling(ax, ticks, side):
    if side == 'x':
        ax.set_xticks((ticks), minor = 1)
        ax.tick_params('x', length=2, width=.5, which='minor')
    elif side == 'y':
        ax.set_yticks((ticks), minor = 1)
        ax.tick_params('y', length=2, width=.5, which='minor') 
        
def annotatefun(ax, textlist, xstart, ystart, ydiff=0.05, fonts=12, col='k', ha='left'):
    counter = 0
    for textstr in textlist:
        ax.text(xstart, (ystart - counter * ydiff), textstr, fontsize=fonts, 
                ha=ha, va='center', transform=ax.transAxes, color=col)
        counter += 1
        
def digit_formatter(inputnumber, digits):  
    return ("{0:0.%sf}"%(digits)).format(round(inputnumber, digits) + 0.0)
    
def symbols_ck(keyword, hspace=-0.25):
    
    if keyword == 'degreeC':
        symb = '$\hspace{-0.1}{^\circ}\hspace{-0.25}$C'
    elif keyword == 'degree':
        symb = '$\hspace{-0.1}{^\circ}\hspace{-0.25}$'
    elif keyword == 'r2':
        symb = 'r$\mathregular{^2} \!$'
    elif keyword == 'km3':
        symb = 'km$\mathregular{^3} \!$'
    elif keyword == 'km2':
        symb = 'km$\mathregular{^2} \!$'
    elif keyword == 'm2':
        symb = 'm$\mathregular{^2} \!$'
    elif keyword == 'm3':
        symb = 'm$\mathregular{^3} \!$'
    elif keyword == 'delta':
        symb = '$\Delta\!\!$ '
    elif keyword == 'mps':
        symb = unicode('m s$\mathregular{^{x1}}\hspace{' + str(hspace) + '}$').replace('x', u'\u2212')
    elif keyword == 'mpd':
        symb = unicode('m d$\mathregular{^{x1}}\hspace{' + str(hspace) + '}$').replace('x', u'\u2212')
    elif keyword == 'mpy':
        symb = unicode('m a$\mathregular{^{x1}}\hspace{' + str(hspace) + '}$').replace('x', u'\u2212')
    elif keyword == 'mwepy':
        symb = unicode('m w.e. a$\mathregular{^{x1}}\hspace{' + str(hspace) + '}$').replace('x', u'\u2212')
    else:
        raise ValueError('WRONG KEYWORD')
        
    return symb  