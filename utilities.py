# -*- coding: utf-8 -*-
"""
Created on Mon Aug 12 2019

@author: Ch.Kienholz
"""

import pandas as pd
import numpy as np
import misc


#%% Variables and helper functions

feet_to_m = 0.3048
feet3_to_m3 = feet_to_m ** 3 # 1 cubic feet = 0.0283168 m3
inch_to_mm = 25.4
m_to_feet = 3.28084

# load lookup tables to convert between runoff and lake elevation and vice versa
runoff_vs_elev = pd.read_pickle('./data/runoff_vs_elev.pickle')
elev_vs_runoff = pd.read_pickle('./data/elev_vs_runoff.pickle')

elev_vs_area_mendenhall = pd.read_pickle('./data/mendenhall_area_lookup.pickle')
   
def foot_to_runoff(foot, elev_vs_runoff):
    meter = foot * feet_to_m 
    return elev_vs_runoff[round(meter, 3)]
    
def runoff_to_foot(m3ps, runoff_vs_elev):
    foot = runoff_vs_elev[round(m3ps, 2)] * m_to_feet 
    return foot 
    
       
#%% Functions to calculate Mendenhall Lake inflow as a function of water level and outflow
                      
def calc_inflow(runoff_mendenhall_series, level_mendenhall_series, 
                lake_area=elev_vs_area_mendenhall, smoothing=0, smoothing_len=60):    
    '''
    Function to calculate discharge into Mendenhall Lake from outflow and lake level
    dV / dt = Q_in - Q_out
    smoothing_length is in minutes
    '''
        
    runoff_mendenhall = np.array(runoff_mendenhall_series) 
    mendenhall_level = np.array(level_mendenhall_series)
    
    # determine time_diff in minutes
    time_diff = int((runoff_mendenhall_series.index[1] - runoff_mendenhall_series.index[0]).total_seconds() / 60.0)
    
    # print '{} minutes'.format(time_diff)
    
    runoff0 = runoff_mendenhall[0:-1]  
    runoff1 = runoff_mendenhall[1:]  
    level0 = mendenhall_level[0:-1] 
    level1 = mendenhall_level[1:]
    
    # calculates level change from one minute to the next
    delta_level = level1 - level0
        
    # create array with lake areas as function of lake level 
    lake_area = np.array([lake_area[round(level, 2)] for level in level1])
    
    # convert the area mof endenhall lake from sqkm to m2, 
    # divide by sampling_interval (minutes) * 60 to get m3 per second  
    deltaV = delta_level * lake_area * 1000 * 1000 / (time_diff * 60)
    
    # mean outflow at minute level
    mean_outflow = (runoff0 + runoff1) / 2.0
    
    mean_inflow = deltaV + mean_outflow
    
    index_inflow = (runoff_mendenhall_series.index[:-1])

    mendenhall_inflow = pd.Series(mean_inflow, index=index_inflow) 
    mendenhall_inflow.name = 'mendenhall_inflow'
    
    if int(smoothing_len / time_diff) < 1:
        
        smoothing = 0
    
    if smoothing == 0: 
    
        return mendenhall_inflow 
        
    else:
        
        mendenhall_inflow_rm = misc.rolling_mean_df(mendenhall_inflow, 
                                                    elements=int(smoothing_len / time_diff))
        mendenhall_inflow_rm.name = 'mendenhall_inflow'
        
        return mendenhall_inflow_rm 
               
def calc_outflow(inflow_forecast_series, outflow_series, lake_area=elev_vs_area_mendenhall, 
                 runoff_vs_elev_lkup=runoff_vs_elev, elev_vs_runoff_lkup=elev_vs_runoff):
    '''
    Function to calculate lake outflow from discharge into Mendenhall Lake 
    Q_out = Q_in - dV / dt
    '''
    
    outflow_previous = outflow_series[inflow_forecast_series.index[0]]
    
    level_previous = runoff_vs_elev_lkup[round(outflow_previous, 2)]
    
    levellist = []
    outflowlist = []
    timelist = []
    
    time_diff = (inflow_forecast_series.index[1] - inflow_forecast_series.index[0]).total_seconds()
    
    for t, inflow in zip(list(inflow_forecast_series.index), list(inflow_forecast_series)):
        
        delta_flow = inflow - outflow_previous
        
        delta_level = delta_flow * time_diff / (lake_area[round(level_previous, 2)] * 1000 ** 2)
        
        level = level_previous + delta_level
         
        outflow = elev_vs_runoff_lkup[round(level, 3)]
        
        levellist.append(level)
        outflowlist.append(outflow)
        timelist.append(t)
        
        outflow_previous = outflow
        level_previous = level
        
    outflow_projected = pd.Series(outflowlist, index=timelist)
    
    return outflow_projected
    
#%%
    
def calc_suicide_volume_2018(df_lookup, start_height, end_height_r, area_std, area_afloat_r, void_space_r):
    '''
    Function to calculate the water volume in Suicide Basin as a function of water height at
    the beginning and end of drainage.
    df_lookup is the lookup table created with the create_lookup_table_SuiB_volume function.
    '''

    volume = 0
    
    # end height as integer
    end_height = int(round(misc.sample_uniform(end_height_r), 0))
    
    # integer steps for the first portion of range (to speed up calculations)
    # start height is water level before lake drains
    if start_height % int(start_height) == 0:    
        l1 = np.arange(end_height, start_height, 1)
        
    else:
        l1 = np.arange(end_height, start_height - 1, 1)
    
    # 10 cm range for rest of range    
    l2 = np.arange(int(start_height), start_height + 0.01, 0.1)

    elev_range = np.concatenate([l1, l2])
    
    end_values = list(elev_range[1:])
    start_values = list(elev_range[0:-1])
    
    # loop over elevation range and integrate volume
    for start_value, end_value in zip(start_values, end_values):
    
        # we work in 1 m and 0.1 m increments, so need to account for this
        bin_height = end_value - start_value 
        
        df_sub = df_lookup.loc[round(start_value + 0.5 * bin_height, 1)]
               
        area_total = misc.sample_gaussian([df_sub['Area_total'], 
                                           df_sub['Area_total'] * area_std / 100.0]) * 1000 ** 2
        ratio = df_sub['ratio'] # how much of the area is afloat
        
        bias_afloat = misc.sample_uniform(area_afloat_r)
        void_space = misc.sample_gaussian(void_space_r)
        
        if void_space > 100:
            void_space = 100
        elif void_space < 0:
            void_space = 0
        
        ratio_adapted = ratio + bias_afloat
        
        if ratio_adapted > 100:
            ratio_adapted = 100
        elif ratio_adapted < 0:
            ratio_adapted = 0
        
        ratio_adapted = ratio_adapted / 100.0
        
        # multiply with bin height to account for different increments                      
        v1 = ratio_adapted * area_total * bin_height 
        v2 = (1.0 - ratio_adapted) * area_total * void_space / 100.0 * bin_height
        
        volume += (v1 + v2) 
    
    # convert to km3
    volume = volume / 1000 ** 3

    return volume   
    
def calc_suicide_volume_2019(start_height, end_height):
    '''
    Function to calculate the water volume in Suicide Basin as a function of 
    water height at the beginning and end of drainage.
    df_lookup is the lookup table created with the create_lookup_table_SuiB_volume 
    function.
    '''
    
    df_lookup = pd.read_pickle('./data/suicide_volume_2019.pickle')
    
    bin_height = 0.1

    volume = 0
    
    elev_range = np.arange(float(end_height), float(start_height), bin_height)
    
    end_values = list(elev_range[1:])
    start_values = list(elev_range[0:-1])
    
    # loop over elevation range and integrate volume
    for start_value, end_value in zip(start_values, end_values):
            
        df_sub = df_lookup.loc[round(start_value + 0.5 * bin_height, 2)]
        
        area_total = df_sub['Area_total'] * 1000 ** 2
                                            
        volume += area_total * bin_height 
    
    # convert to km3
    volume = volume / 1000 ** 3

    return volume

def run_monte_carlo_volume_calcs(start_height):
     
    volume_list = []
    
    df_lookup = pd.read_pickle('./data/suicide_volume_2018.pickle')
    
    print 'Start elevation used for MC calculations: {} m'.format(start_height)
        
    end_height_r = [382, 390] # range in m, for gaussian sampling
    area_std = 5 # std in % for each area estimate for gaussian sampling    
    area_afloat_r = [-20, 20] # uncertainty in % for area_afloat estimate, for uniform sampling
    void_space_r = [25, 10] # voidspace in % in areas no afloat, mean and std, for gaussian sampling
    
    for i in range(0, 2500):
               
        vol = calc_suicide_volume_2018(df_lookup, start_height, end_height_r, 
                                       area_std, area_afloat_r, void_space_r)
        
        volume_list.append(vol)
        
    return volume_list     